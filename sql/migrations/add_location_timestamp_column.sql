ALTER TABLE user_locations
ADD COLUMN "last_present_timestamp" TIMESTAMP DEFAULT now();