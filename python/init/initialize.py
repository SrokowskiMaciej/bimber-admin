from enum import Enum
import os
import pyrebase
import firebase_admin
from firebase_admin import credentials


class Environment(Enum):
    STAGING_LOCAL = 1
    STAGING_CLOUD = 2
    PRODUCTION = 3


# SET ENVIRONMENT YOU NEED TO USE HERE
current_environment = Environment.PRODUCTION

init_file_path = os.path.dirname(os.path.realpath(__file__))

STAGING_API_KEY_PATH = f"{init_file_path}/credentials/bimber-staging-api-key.txt"
STAGING_MIGRATION_TOKEN_PATH = f"{init_file_path}/credentials/bimber-staging-migration-auth-token.txt"
STAGING_FIREBASE_CREDENTIALS_PATH = f"{init_file_path}/credentials/bimber-staging.json"

PRODUCTION_API_KEY_PATH = f"{init_file_path}/credentials/bimber-a8ebe-api-key.txt"
PRODUCTION_MIGRATION_TOKEN_PATH = f"{init_file_path}/credentials/bimber-a8ebe-migration-auth-token.txt"
PRODUCTION_FIREBASE_CREDENTIALS_PATH = f"{init_file_path}/credentials/bimber-a8ebe.json"

STAGING_LOCAL_DOMAIN = "http://localhost:5432"
STAGING_CLOUD_DOMAIN = "http://bimber-staging.appspot.com"
PRODUCTION_DOMAIN = "http://bimber-a8ebe.appspot.com"

if current_environment == Environment.STAGING_LOCAL or current_environment == Environment.STAGING_CLOUD:
    api_key_set = os.path.exists(STAGING_API_KEY_PATH)
    if (not api_key_set):
        raise Exception("Api key not set. Please add it in: " + STAGING_API_KEY_PATH)
    migration_auth_token_set = os.path.exists(STAGING_MIGRATION_TOKEN_PATH)
    if (not migration_auth_token_set):
        raise Exception("Migration token not set. Please add it in: " + STAGING_MIGRATION_TOKEN_PATH)
    firebase_credentials_set = os.path.exists(STAGING_FIREBASE_CREDENTIALS_PATH)
    if (not firebase_credentials_set):
        raise Exception("Firebase credentials not set. Please add it in: " + STAGING_MIGRATION_TOKEN_PATH)
elif current_environment == Environment.PRODUCTION or current_environment == Environment.STAGING_CLOUD:
    api_key_set = os.path.exists(PRODUCTION_API_KEY_PATH)
    if (not api_key_set):
        raise Exception("Api key not set. Please add it in: " + PRODUCTION_API_KEY_PATH)
    migration_auth_token_set = os.path.exists(PRODUCTION_MIGRATION_TOKEN_PATH)
    if (not migration_auth_token_set):
        raise Exception("Migration token not set. Please add it in: " + PRODUCTION_MIGRATION_TOKEN_PATH)
    firebase_credentials_set = os.path.exists(PRODUCTION_FIREBASE_CREDENTIALS_PATH)
    if (not firebase_credentials_set):
        raise Exception("Firebase credentials not set. Please add it in: " + PRODUCTION_FIREBASE_CREDENTIALS_PATH)
else:
    raise Exception("No such environment")

if current_environment == Environment.STAGING_LOCAL:
    credentials_file_path = STAGING_FIREBASE_CREDENTIALS_PATH
    config = {
        "apiKey": open(STAGING_API_KEY_PATH, 'r').read(),
        "authDomain": "bimber-staging.firebaseapp.com",
        "databaseURL": "https://bimber-staging.firebaseio.com/",
        "storageBucket": "gs://bimber-staging.appspot.com",
        "serviceAccount": STAGING_FIREBASE_CREDENTIALS_PATH
    }
    migration_auth_token = open(STAGING_MIGRATION_TOKEN_PATH, 'r').read()
    api_domain = STAGING_LOCAL_DOMAIN
elif current_environment == Environment.STAGING_CLOUD:
    credentials_file_path = STAGING_FIREBASE_CREDENTIALS_PATH
    config = {
        "apiKey": open(STAGING_API_KEY_PATH, 'r').read(),
        "authDomain": "bimber-staging.firebaseapp.com",
        "databaseURL": "https://bimber-staging.firebaseio.com/",
        "storageBucket": "gs://bimber-staging.appspot.com",
        "serviceAccount": STAGING_FIREBASE_CREDENTIALS_PATH
    }
    migration_auth_token = open(STAGING_MIGRATION_TOKEN_PATH, 'r').read()
    api_domain = STAGING_CLOUD_DOMAIN
elif current_environment == Environment.PRODUCTION:
    credentials_file_path = PRODUCTION_FIREBASE_CREDENTIALS_PATH
    config = {
        "apiKey": open(PRODUCTION_API_KEY_PATH, 'r').read(),
        "authDomain": "bimber-a8ebe.firebaseapp.com",
        "databaseURL": "https://bimber-a8ebe.firebaseio.com/",
        "storageBucket": "gs://bimber-a8ebe.appspot.com",
        "serviceAccount": PRODUCTION_FIREBASE_CREDENTIALS_PATH
    }
    migration_auth_token = open(PRODUCTION_MIGRATION_TOKEN_PATH, 'r').read()
    api_domain = PRODUCTION_DOMAIN
else:
    raise Exception("No such environment")

firebase = pyrebase.initialize_app(config)
cred = credentials.Certificate(credentials_file_path)
default_app = firebase_admin.initialize_app(cred)
admin = firebase_admin

