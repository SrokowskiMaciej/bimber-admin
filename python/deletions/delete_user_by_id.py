from python.init import initialize

db = initialize.firebase.database()

uId = "2gPOwItBmwQyEYZhF12zHsDzFGz2"
db.child("users").child(uId).remove()
db.child("photos").child(uId).remove()
db.child("user_locations").child(uId).remove()
db.child("emails").child(uId).remove()
