from python.init import initialize
import time

db = initialize.firebase.database()

activeParties = db.child("groups").order_by_child("discoverable").equal_to(True).get().val().items()
currentTime = time.time() * 1000
print(len(activeParties))
for party_id, party in activeParties:
    # print(f"Party: {party}")
    overdue = currentTime - party["meetingTime"];
    overdueHours = overdue/3600000;
    if overdue >  3600 * 6 * 1000:
        geolocation = db.child("geomatching").child("group_locations").child(party_id).remove()
        # if geolocation is not None:
        #     print(f"Visible, overdue by : {overdueHours}h Party: {party_id}, {party}")
        # else:
        #     print(f"Not visible, overdue by : {overdueHours}h Party: {party_id}, {party}")
