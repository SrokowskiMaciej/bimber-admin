from python.init import initialize
import pprint

db = initialize.firebase.database()

u_id = "tNBHLiXFXgTULeYQttKbvq7TS6w1"
user_evaluations = db.child("evaluated_persons").child(u_id).get()

for evaluated_uid, evaluation in list(user_evaluations.val().items()):
    status = evaluation["status"]
    pprint.pprint(f"{evaluated_uid}: {status}")
