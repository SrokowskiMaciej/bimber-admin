from python.init import initialize
import asyncio

db = initialize.firebase.database()


async def updateFriends(uid, total):
    evaluation = db.child("evaluated_persons").child(uid).get().val()
    if evaluation is not None:
        friends = 0
        for evaluated_id, evaluation_status in reversed(list(evaluation.items())):
            if (evaluation_status['status'] == "MATCHED"):
                friends += 1
        updateFriends.users_updated += 1
        current_updated_users = updateFriends.users_updated
        print(f"{current_updated_users} of {total} users")
        print(f"{uid} friends: {friends}")
        user = db.child("users").child(uid).get().val()
        if user is not None:
            db.child("users").child(uid).child("friends").set(friends)


async def run():
    users = db.child("users").shallow().get().val()
    users_total = len(users)
    tasks = []
    for uid in reversed(list(users)):
        task = asyncio.ensure_future(updateFriends(uid, users_total))
        tasks.append(task)
    wait_tasks = await asyncio.gather(*tasks)


updateFriends.users_updated = 0
ioloop = asyncio.get_event_loop()
ioloop.run_until_complete(run())
ioloop.close()
