from python.init import initialize
import pprint

db = initialize.firebase.database()

users_memberships = db.child("memberships_user_aggregated").get()

users_connections = {}

for user_memberships in users_memberships.each():
    user_id = user_memberships.key()
    users_connections[user_id + "/friends"] = 0
    users_connections[user_id + "/parties"] = 0
    for membership in list(user_memberships.val().values()):
        if membership["membershipStatus"] == "ACTIVE":
            if membership["chatType"] == "DIALOG":
                users_connections[user_id + "/friends"] = users_connections[user_id + "/friends"] + 1
            elif membership["chatType"] == "GROUP":
                users_connections[user_id + "/parties"] = users_connections[user_id + "/parties"] + 1

pprint.pprint(users_connections)
# TODO Uncomment if you want it to run, just a security measure
# db.child("users").update(users_connections)
