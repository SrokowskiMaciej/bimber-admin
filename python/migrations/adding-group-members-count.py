from python.init import initialize
import pprint

db = initialize.firebase.database()

memberships = db.child("memberships_chat_aggregated").get()

group_members_counts = {}

for chat_memberships in memberships.each():
    group_id = chat_memberships.key()
    for membership in list(chat_memberships.val().values()):
        if membership["chatType"] == "GROUP" and membership["membershipStatus"] == "ACTIVE":
            if group_id + "/membersCount" in group_members_counts:
                group_members_counts[group_id + "/membersCount"] = group_members_counts[group_id + "/membersCount"] + 1
            else:
                group_members_counts[group_id + "/membersCount"] = 1
pprint.pprint(group_members_counts)
# TODO Uncomment if you want it to run, just a security measure
# db.child("groups").update(group_members_counts)
