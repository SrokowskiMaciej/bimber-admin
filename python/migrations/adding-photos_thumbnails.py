from python.init import initialize
import pprint
import requests
import os
from _datetime import datetime
from PIL import Image

firebase = initialize.firebase
firebase_admin_storage = initialize.storage
db = firebase.database()
storage = firebase.storage()

directory = "images/"

if not os.path.exists(directory):
    os.makedirs(directory)

users_photos = db.child("photos").get()

for user_photos in users_photos.each():
    user_id = user_photos.key()
    for photo_id, photo in user_photos.val().items():
        pprint.pprint(photo)
        if "uri" in photo:
            response = requests.get(photo["uri"], stream=True)
            response.raw.decode_content = True  # handle spurious Content-Encoding
            img = Image.open(response.raw)
            img.thumbnail((100, 100))
            local_photo_path = directory + photo_id
            img.save(local_photo_path, 'JPEG')
            firebase_admin_storage.bucket().blob(
                "photos/profiles/" + user_id + "/thumb_" + photo_id).upload_from_filename(filename=local_photo_path,
                                                                                          content_type="image/jpeg")
            signed_url = firebase_admin_storage.bucket().blob(
                "photos/profiles/" + user_id + "/thumb_" + photo_id).generate_signed_url(datetime(2500, 1, 1))
            print("Uploaded thumbnail - user: " + user_id + ", photo: " + photo_id + ", url: " + signed_url + "\n")
            db.child("photos").child(user_id).child(photo_id).child("thumb").set(signed_url)
