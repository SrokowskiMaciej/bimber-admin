from python.init import initialize

db = initialize.firebase.database()

messages = db.child("messages").get()

message_user_names = {}
message_count = 0

for chat_messages in messages.each():
    chat_id = chat_messages.key()
    for message_id, message in list(chat_messages.val().items()):
        message_count = message_count + 1
        user_name = db.child("users").child(message["userId"]).child("displayName").get().val()
        path = chat_id + "/" + message_id + "/userName"
        message_user_names[path] = user_name
        print("Path: {} User name: {} Message count: {}".format(path, user_name, message_count))
        # TODO Uncomment if you want it to run, just a security measure
        # db.child("messages").child(chat_id).child(message_id).child("userName").set(user_name)
