from python.init import initialize
import pprint

db = initialize.firebase.database()

group_join_requests = db.child("group_join_requests").get()

group_join_requests_user_aggregated = {}

for group_join_request in group_join_requests.each():
    groupId = group_join_request.key()
    print("JOIN REQUESTS FOR GROUP: {}".format(groupId))
    pprint.pprint(group_join_request.val())
    for userId, join_request in group_join_request.val().items():
        if userId in group_join_requests_user_aggregated:
            group_join_requests_user_aggregated[userId][groupId] = join_request
        else:
            group_join_requests_user_aggregated[userId] = {groupId: join_request}

pprint.pprint(group_join_requests_user_aggregated)
# TODO Uncomment if you want it to run, just a security measure
# db.child("group_join_requests_user_aggregated").update(group_join_requests_user_aggregated)
