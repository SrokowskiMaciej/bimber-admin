from python.init import initialize
import pprint

db = initialize.firebase.database()

users = db.child("users").get()

users_initial_credentials = {}

for user in users.each():
    user_id = user.key()
    users_initial_credentials[user_id + "/providerType"] = "facebook.com"
pprint.pprint(users_initial_credentials)
# TODO Uncomment if you want it to run, just a security measure
# db.child("initial_user_credentials").update(users_initial_credentials)
