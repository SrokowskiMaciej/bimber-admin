from python.init import initialize
import json
import asyncio
from aiohttp import ClientSession


def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


async def sendRequest(session, evaluating_uid, user_evaluations):
    headers = {
        "Authorization": initialize.migration_auth_token,
        "Content-Type": "application/json"
    }
    json_data = json.dumps(user_evaluations, ensure_ascii=False)
    uri = f"{initialize.api_domain}/migration/evaluation/batch/{evaluating_uid}"
    async with session.put(uri, data=json_data, headers=headers, timeout=100000) as response:
        finished_response = await response.read()
        sendRequest.sent_counter += 1
        current_sent = sendRequest.sent_counter
        print(f"{current_sent}: {finished_response}")
        return finished_response


async def run():
    db = initialize.firebase.database()

    evaluations = db.child("evaluated_persons").get()
    requests_total = len(list(evaluations.val().items()))
    requests_aggregated = 0
    evaluations_aggregated = 0

    for chunk in chunks(evaluations.each(), 100):
        tasks = []
        async with ClientSession(read_timeout=100000, conn_timeout=100000) as session:
            for evaluation in chunk:

                evaluation_id = evaluation.key()
                evaluation_values = {}
                for evaluated_id, evaluation_status in list(evaluation.val().items()):
                    if (evaluation_status['status'] == "MATCHED"):
                        evaluation_values[evaluated_id] = "LIKED"
                        evaluations_aggregated += 1
                    elif (evaluation_status['status'] == "NONE"):
                        print("Do nothing")
                    elif (evaluation_status['status'] == "LIKED" or evaluation_status['status'] == "DISLIKED"):
                        evaluation_values[evaluated_id] = evaluation_status['status']
                        evaluations_aggregated += 1
                requests_aggregated = requests_aggregated + 1
                print(f"{requests_aggregated} of {requests_total} requests")
                print(f"{evaluations_aggregated} evaluations")
                task = asyncio.ensure_future(sendRequest(session, evaluation_id, evaluation_values))
                tasks.append(task)
            responses = await asyncio.gather(*tasks, return_exceptions=True)
            print(responses)


sendRequest.sent_counter = 0
loop = asyncio.get_event_loop()
future = asyncio.ensure_future(run())
loop.run_until_complete(future)
