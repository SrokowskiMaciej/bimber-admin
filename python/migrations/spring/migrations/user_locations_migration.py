import json
import requests
from python.init import initialize


def send_request(location_uid, location_req_params):
    headers = {
        "Authorization": initialize.migration_auth_token,
        "Content-Type": "application/json"
    }
    json_data = json.dumps(location_req_params, ensure_ascii=False).encode("utf-8")
    request = requests.put(f"{initialize.api_domain}/migration/usersLocations/{location_uid}", data=json_data,
                           headers=headers)
    print(json_data)
    print(request.text)


db = initialize.firebase.database()

user_locations = list(db.child("user_locations").get().val().items())
user_locations_total = len(user_locations)
user_locations_sent = 0
for location_id, location in user_locations:
    location_params = {}
    location_params["locationName"] = location["locationName"]
    location_params["latitude"] = location["latitude"]
    location_params["longitude"] = location["longitude"]
    location_params["range"] = location["range"]
    location_params["type"] = location["type"]
    user_locations_sent = user_locations_sent + 1
    print(f"{user_locations_sent} of {user_locations_total}")
    send_request(location_id, location_params)
