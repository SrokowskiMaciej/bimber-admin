import json
import workspace
import os

from python.init import initialize

db = initialize.firebase.database()
evaluations = db.child("evaluated_persons").get()

evaluations_csv = ""
# os.remove(workspace.workspace_path + "/evaluations.csv")
with open(workspace.workspace_path + "/evaluations.csv", "a") as myfile:
    myfile.write("evaluating_uid,evaluated_uid,status\n")
    for evaluating_uid, evaluations in evaluations.val().items():
        for evaluated_uid, evaluation in evaluations.items():
            if (evaluation['status'] == "MATCHED"):
                myfile.write(evaluating_uid + "," + evaluated_uid + ",LIKED" + "\n")
            elif (evaluation['status'] == "NONE"):
                print("Do nothing")
            elif (evaluation['status'] == "LIKED" or evaluation['status'] == "DISLIKED"):
                myfile.write(evaluating_uid + "," + evaluated_uid + "," + evaluation['status'] + "\n")
