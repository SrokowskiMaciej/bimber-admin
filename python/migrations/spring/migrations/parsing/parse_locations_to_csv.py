import json
import workspace
import os

database_path = open(workspace.workspace_path + "/bimber-database.json")
bimber_database = json.load(database_path)
evaluations_csv = ""

os.remove(workspace.workspace_path + "/locations.csv")
with open(workspace.workspace_path + "/locations.csv", "a") as myfile:
    myfile.write("location_uid,latitude,longitude,range,type,location_name\n")
    for uid, location in bimber_database["user_locations"].items():
        name_ = location["locationName"]
        myfile.write(uid + ",\"" +
                     format(location["latitude"], 'f') + "\",\"" +
                     format(location["longitude"], 'f') + "\"," +
                     format(location["range"], 'f') + "," +
                     location["type"] + ",\"" +
                     name_ + "\"\n")
