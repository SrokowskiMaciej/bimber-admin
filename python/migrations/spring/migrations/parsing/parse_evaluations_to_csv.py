import json
import workspace
import os

database_path = open(workspace.workspace_path + "/bimber-database.json")
bimber_database = json.load(database_path)
evaluations_csv = ""
# os.remove(workspace.workspace_path + "/evaluations.csv")
with open(workspace.workspace_path + "/evaluations.csv", "a") as myfile:
    myfile.write("evaluating_uid,evaluated_uid,status\n")
    for evaluating_uid, evaluations in bimber_database["evaluated_persons"].items():
        for evaluated_uid, evaluation in evaluations.items():
            if (evaluation['status'] == "MATCHED"):
                myfile.write(evaluating_uid + "," + evaluated_uid + ",LIKED" + "\n")
            elif (evaluation['status'] == "NONE"):
                print("Do nothing")
            elif (evaluation['status'] == "LIKED" or evaluation['status'] == "DISLIKED"):
                myfile.write(evaluating_uid + "," + evaluated_uid + "," + evaluation['status'] + "\n")
