from python.init import initialize
import requests
import json


def sendRequest(evaluating_uid, user_evaluations):
    headers = {
        "Authorization": initialize.migration_auth_token,
        "Content-Type": "application/json"
    }
    json_data = json.dumps(user_evaluations, ensure_ascii=False)
    uri = f"{initialize.api_domain}/migration/evaluation/batch/{evaluating_uid}"
    request = requests.put(uri, data=json_data,
                           headers=headers)
    print(json_data)
    print(request.text)


db = initialize.firebase.database()

evaluations = db.child("evaluated_persons").shallow().get().val()
evaluations_total = len(evaluations)
evaluations_sent = 0
for evaluation in evaluations:
    evaluation_values = {}
    for evaluated_id, evaluation_status in list(db.child("evaluated_persons").child(evaluation).get().val().items()):
        if (evaluation_status['status'] == "MATCHED"):
            evaluation_values[evaluated_id] = "LIKED"
        elif (evaluation_status['status'] == "NONE"):
            print("Do nothing")
        elif (evaluation_status['status'] == "LIKED" or evaluation_status['status'] == "DISLIKED"):
            evaluation_values[evaluated_id] = evaluation_status['status']
    evaluations_sent = evaluations_sent + 1
    print(f"{evaluations_sent} of {evaluations_total}")
    sendRequest(evaluation, evaluation_values)
