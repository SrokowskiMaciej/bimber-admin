import requests
from python.init import initialize

for evaluatingId in range(0, 5000):
    for evaluatedId in range(0, 5000):
        data = '''{
	    "status": "LIKE"
        }
        '''
        headers = {
            "Authorization": initialize.migration_auth_token,
            "Content-Type": "application/json"
        }
        requests.put(f"{initialize.api_domain}/migration/evaluation/{evaluatingId}/{evaluatedId}", data=data, headers=headers)
        print(f"Added evaluation for evaluatingId: {evaluatingId} and evaluatedId: {evaluatedId}")
