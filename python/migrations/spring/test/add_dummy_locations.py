import requests
from python.init import initialize

for uId in range(0, 10000):
    data = '''{
	    "latitude": "0",
	    "longitude": "0",
	    "range": "1",
	    "type": "CURRENT_LOCATION",
	    "locationName": "FFDSFE"
    }
    '''
    headers = {
        "Authorization": initialize.migration_auth_token,
        "Content-Type": "application/json"
    }
    requests.put(f"{initialize.api_domain}/migration/usersLocations/{uId}", data=data, headers=headers)
    print(f"Added location for uid: {uId}")
