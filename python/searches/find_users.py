from python.init import initialize
import pprint

db = initialize.firebase.database()

users = db.child("users").get()

for user_id, user in list(users.val().items()):
    if (("fullName" in user) and ("Kachel" in user["fullName"])):
        pprint.pprint("User id: {} value: {}".format(user_id, user))
