from python.init import initialize
import pprint

db = initialize.firebase.database()

emails = db.child("emails").get()

for user_id, email in list(emails.val().items()):
    if ("weronikawojcik321@gmail.com" in email):
        pprint.pprint("User id: {} email: {}".format(user_id, email))
