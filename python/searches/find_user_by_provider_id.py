from python.init import initialize
from firebase_admin import auth

# Iterate through all users. This will still retrieve users in batches,
# buffering no more than 1000 users in memory at a time.
for user in auth.list_users().iterate_all():
    print('User ' + user.uid)
    for user_info in user.provider_data:
        print('Provider data: ' + user_info.provider_id)


