from python.init import initialize
import pprint

db = initialize.firebase.database()

groups = db.child("groups").get()

for group_id, group in list(groups.val().items()):
    if (group["name"] == "Bar Krzemieniec"):
        pprint.pprint("Group id: {} value: {}".format(group_id, group))