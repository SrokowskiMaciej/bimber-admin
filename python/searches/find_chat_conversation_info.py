from python.init import initialize
import pprint

db = initialize.firebase.database()

chat_id = "-L6ffNVqXH_HXr5CXO3k"
messages = db.child("messages").child(chat_id).get()

for message_id, message in list(messages.val().items()):
    if (message["content"]["contentType"] == "TEXT"):
        pprint.pprint("{} value: {}".format(message["userName"], message["content"]["text"]))
    # if (group["name"] == "Bar Krzemieniec"):
