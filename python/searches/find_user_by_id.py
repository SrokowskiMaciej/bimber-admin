from python.init import initialize
import pprint

db = initialize.firebase.database()

uId = "nIRjW3tNh5gHJjcx8oPNrlkSb5O2"
user = db.child("users").child(uId).get()
photos = db.child("photos").child(uId).get()
user_location = db.child("user_locations").child(uId).get()
user_chats = db.child("memberships_user_aggregated").child(uId).get()
email = db.child("emails").child(uId).get()

pprint.pprint("Profile: {}".format(user.val()))
pprint.pprint("Photos: {}".format(photos.val()))
pprint.pprint("Locations: {}".format(user_location.val()))
pprint.pprint("Email: {}".format(email.val()))

# pprint.pprint("USER DIALOG: {}".format(user_chats.val()))
active_dialogs = {v["chatId"] for k, v in user_chats.val().items() if v["chatType"] == "DIALOG" and v["membershipStatus"] == "ACTIVE"}
expired_dialogs = {v["chatId"] for k, v in user_chats.val().items() if v["chatType"] == "DIALOG" and v["membershipStatus"] == "EXPIRED"}
active_parties = {v["chatId"] for k, v in user_chats.val().items() if v["chatType"] == "GROUP" and v["membershipStatus"] == "ACTIVE"}
expired_parties = {v["chatId"] for k, v in user_chats.val().items() if v["chatType"] == "GROUP" and v["membershipStatus"] == "EXPIRED"}


pprint.pprint("ACTIVE DIALOGS: {}".format(active_dialogs))
pprint.pprint("EXPIRED DIALOGS: {}".format(expired_dialogs))
pprint.pprint("ACTIVE PARTIES: {}".format(active_parties))
pprint.pprint("EXPIRED PARTIES: {}".format(expired_parties))
